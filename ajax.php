<?php
  
add_action( 'wp_enqueue_scripts', 'optinFormScripts' );

function optinFormScripts() 
{
	wp_enqueue_script( 'optin-form-ajax', plugins_url( '/js/ajax.js', __FILE__ ), array('jquery'), '1.0', true );
	
	wp_localize_script( 'optin-form-ajax', 'OPTINFORM', array(
		'ajaxUrl' => admin_url( 'admin-ajax.php' )
	));	

	wp_enqueue_style( 'optin-form-styles', plugins_url( '/css/optin.css', __FILE__ ), array(), '1.0', 'screen' );

}

add_action( 'wp_ajax_nopriv_optin_form_submit', 'optinFormSubmit' );
add_action( 'wp_ajax_optin_form_submit', 'optinFormSubmit' );

function optinFormSubmit() 
{
  $optin_form_id  = $_POST['of-id'];
  $page_id        = $_POST['page-id'];
  $name           = isset($_POST['of-name']) ? $_POST['of-name'] : false;
  $email          = $_POST['of-email'];
  
  $response = array(
    'success' => false,
  );
  
  if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) 
  { 
    $optin_form   = get_post( $optin_form_id );
    $page         = get_post( $page_id );
    $locked_type  = get_field( 'locked_type', $optin_form->ID );
    
    switch ( $locked_type )
    {
      case 'page':
        $response = array(
          'success'   => true,
          'redirect'  => get_permalink( get_field( 'locked_page', $optin_form->ID )->ID ),
        );
        break;
      case 'download':
        $response = array(
          'success'   => true,
          'redirect'  => get_field( 'locked_download', $optin_form->ID )['url'],
        );
        break;
    } 
    
    // send email to admin
    
    $to       = get_field( 'form_recipient', $optin_form->ID ) ? get_field( 'form_recipient', $optin_form->ID ) : get_option('admin_email');
    $subject  = '[Opt-in form] Submission from ' . get_the_title( $optin_form->ID );
    $message  = 'Form submitted from "' . get_the_title($page->ID) . '" (' . get_permalink($page->ID) . ')' . "\n\n" . 'Email: ' . $email . ( $name ? "\n\n" . 'Name: ' . $name : '' ); 
    
    wp_mail(
      $to,
      $subject,
      $message
    );
    
  }

	echo json_encode($response);
	die();    
	
}