<?php
  
add_action( 'init', 'cptui_register_my_cpts_optin_form' );
function cptui_register_my_cpts_optin_form() {
	$labels = array(
		"name" => __( 'Opt-in forms', 'strat-coach-podcasts' ),
		"singular_name" => __( 'Opt-in form', 'strat-coach-podcasts' ),
		);

	$args = array(
		"label" => __( 'Opt-in forms', 'strat-coach-podcasts' ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
				"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "optin_form", "with_front" => true ),
		"query_var" => true,
		"menu_icon" => "dashicons-carrot",
		"supports" => array( "title" ),					);
	register_post_type( "optin_form", $args );

// End of cptui_register_my_cpts_optin_form()
}
  