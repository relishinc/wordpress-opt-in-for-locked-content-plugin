<?php

/**
 * Shortcode
 */

function optinFormShortcode( $atts ) 
{
  $of = new OptinForm();
  
	$atts = shortcode_atts( array(
		'id' => false,
	), $atts, 'optin-form' );
	
	$optin_form = get_post($atts['id']);
	
	if ( $optin_form && $of->dependenciesMet() )
	{
  	include('forms/default.php');
	}

}

add_shortcode( 'optin-form', 'optinFormShortcode' );