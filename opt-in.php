<?php 
/**
 * Plugin Name: Opt-in for locked content
 * Plugin URI: https://relishinc.bitbucket.io
 * Description: This plugin lets you collect names and emails and reward users with locked content
 * Version: 0.0.1
 * Author: Steve Palmer / Relish
 * Author URI: https://reli.sh
 * License: GPLv3
 */

/**
 * Dependency check
 */

function init_optinForm() 
{
  return new OptinForm();
}

if ( is_admin() )
{
  add_action( 'plugins_loaded', 'init_optinForm' );
}

class OptinForm
{

  public function __construct()
  {
  	if ( ! $this->dependenciesMet() ) 
  	{
      add_action( 'admin_notices', array( &$this, 'showDependencyWarning' ) );  	
      add_action( 'add_meta_boxes', array( &$this, 'addMetaBoxes' ) );
  	}
  }
  
  public function dependenciesMet()
  {
    return ( class_exists('acf') );
  }

  public function addMetaBoxes()
  {
    add_meta_box( 
       'optin-form-dependency-warning-metabox'
      ,__( 'Notice' )
      ,array( &$this, 'renderDependencyWarningMetaBox' )
      ,'optin_form' 
      ,'advanced'
      ,'high'
    );
  }

  public function renderDependencyWarningMetaBox() 
  {
    ?>
    <div>
      <?php echo $this->_getDependencyWarningMessage(); ?>
    </div>
    <?php
  }
  
  public function showDependencyWarning()
  {
    ?>
    <div class="notice notice-success is-dismissible">
      <?php echo $this->_getDependencyWarningMessage(); ?>
    </div>
    <?php    
  }
  
  protected function _getDependencyWarningMessage()
  {
    return '<p><strong>Opt-in for locked content</strong> requires that the <a href="' . admin_url('plugin-install.php?s=Advanced+Custom+Fields&tab=search&type=term') .'">Advanced Custom Fields</a> plugin be installed. It will not work properly until it is.</p>';
  }
  
}

/**
 * Custom post type and ACF
 */

require( dirname( __FILE__ ) . '/inc/cpt.php' );
require( dirname( __FILE__ ) . '/inc/acf.php' );

/**
 * Admin class
 */

require( dirname( __FILE__ ) . '/admin.php' );

/**
 * Shortcode
 */
 
require( dirname( __FILE__ ) . '/shortcode.php' );

/**
 * AJAX
 */
 
require( dirname( __FILE__ ) . '/ajax.php' );

