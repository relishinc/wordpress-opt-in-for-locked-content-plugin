<div class="optin-form optin-form--<?php echo $optin_form->post_name; ?>">
  <form method="post">
    <input type="hidden" name="action" value="optin_form_submit">
    <input type="hidden" name="of-id" value="<?php echo $optin_form->ID; ?>">
    <input type="hidden" name="page-id" value="<?php the_ID(); ?>">
    
    <?php if ( get_field( 'form_image', $optin_form->ID ) ): ?>
    <div class="form-image">
      <div class="image" style="background-image: url('<?php echo get_field( 'form_image', $optin_form->ID )['sizes']['medium']; ?>');"></div>
    </div>
    <?php endif; ?>
    
    <div class="form-content">
      
      <h2 class="title"><?php the_field( 'form_title', $optin_form->ID ); ?></h2>
      <?php if ( get_field( 'form_description', $optin_form->ID ) ): ?>
      <div class="description"><?php the_field( 'form_description', $optin_form->ID ); ?></div>
      <?php endif; ?>
      
      <div class="form-fields">
      
        <?php if ( get_field( 'form_ask_for_name', $optin_form->ID ) ): ?>
        <div class="form-input">
          <label for="of-name">Name</label>
          <input type="text" name="of-name" placeholder="Enter your name" id="of-name" required="required">
        </div>
        
        <?php endif; ?>
        <div class="form-input">
          <label for="of-email">Email</label>
          <input type="email" name="of-email" placeholder="Enter your email" id="of-email" required="required">
        </div>
        
        <button type="submit" class="form-btn"><?php the_field( 'form_call_to_action', $optin_form->ID ); ?></button>
      
      </div>
      
    </div><!-- .form-content -->
    
  </form>
</div>