# Opt-in for Locked Content Plugin

A WordPress plugin that lets you insert a form onto a page to collect names and emails from users in exchange for access to locked content (an eBook chapter, some special private page, etc). 

### Installation

Grab the latest version here: 

https://bitbucket.org/relishinc/opt-in-for-locked-content-plugin/get/HEAD.zip

Unzip it and put it in your ```/wp-content/plugins``` folder. Activate the plugin and go. There are no settings.

This plugin requires that [Advanced Custom Fields](http://wordpress.org/plugins/advanced-custom-fields/) be installed. If it's not, it won't really work.

### Usage

This plugin creates an "Opt-in form" custom post type. Populate the form with a title, description, button label, an image of the product you're giving away, what locked content users get for signing up, etc.

To place the form onto a page use the shortcode:

```php
[optin-form id=XX]
```

Or in a template file like this:

```php
<?php echo do_shortcode('[optin-form id=XX]'); ?>
```

Where XX is the ID of the Opt-in Form in the admin. When you're creating a form in the admin the plugin will display the shortcode right there for you. Easy peasy.
