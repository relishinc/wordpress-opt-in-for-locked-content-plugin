<?php

/**
 * Admin class
 */
 
function init_optinFormAdmin() 
{
  return new OptinFormAdmin();
}

if ( is_admin() )
{
  add_action( 'load-post.php', 'init_optinFormAdmin' );
}

class OptinFormAdmin
{

  public function __construct()
  {
    add_action( 'add_meta_boxes', array( &$this, 'addMetaBoxes' ) );
  }

  public function addMetaBoxes()
  {
    add_meta_box( 
       'optin-form-shortcode-metabox'
      ,__( 'Shortcode' )
      ,array( &$this, 'renderShortcodeMetaBox' )
      ,'optin_form' 
      ,'side'
      ,'default'
    );
  }

  public function renderShortcodeMetaBox() 
  {
    ?>
    <div>
      <p>Shortcode for this form:</p>
      <input type="text" value="[optin-form id=<?php the_ID(); ?>]" style="width: 100%;">
      <p>Or in a template:</p>
      <code>&lt;?php echo do_shortcode('[optin-form id=<?php the_ID(); ?>]'); ?&gt;</code>
    </div>
    <?php

  }
  
}