jQuery(document).ready(function($) {
  
  $('.optin-form form')
    .prepend('<div class="form-loader"><div class="loader"></div></div>')
    .on('submit', function(e) {
      e.preventDefault();
      
      var
        form      = $(this),
        formData  = form.serialize();
      
      // Clear alerts
        
      form
        .find('.alert-message')
        .remove();
        
      // Disable form
      
      form
        .addClass('thinking')
        .find(':input')
        .prop('disabled', true);
        
      // Submit data
      
    	$.getJSON({
      		url:  OPTINFORM.ajaxUrl,
      		type: 'post',
      		data: formData
      	})      
      	.done(function(response) {

          form  
            .removeClass('thinking')
            .find(':input')
            .prop('disabled', false);
                    	
        	if ( response.success )
        	{
          	form
          	  .find('.form-fields')
          	  .hide();
          	
          	if ( response.redirect )
          	{
              form
                .find('.form-fields')
                .before('<div class="alert-message success waiting">Thank you for signing up! Redirecting...</div>');
              
              setTimeout(function() {
                window.location = response.redirect;
              }, 1500)
            	
          	}
          	else
          	{
              form
                .find('.form-fields')
                .before('<div class="alert-message success">Thank you, we&rsquo;ll be in touch shortly!</div>');
          	}
        	}
        	else
        	{
            form
              .find('.form-fields')
              .before('<div class="alert-message error">There was a problem. Please try again!</div>');
        	}
        	
      	});
      
      return false;
      
    });
  
});