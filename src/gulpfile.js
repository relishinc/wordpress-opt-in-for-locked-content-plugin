var 
  gulp            = require('gulp'),
  gutil           = require('gulp-util'),
  bower           = require('bower'),
  concat          = require('gulp-concat'),
  header          = require('gulp-header'),
  minifyCss       = require('gulp-minify-css'),
  rename          = require('gulp-rename'),
  sh              = require('shelljs'),
  less            = require('gulp-less'),
  uglify          = require('gulp-uglify'),
  sourcemaps      = require('gulp-sourcemaps'),
  livereload      = require('gulp-livereload')
  ;

var 
  paths = {
    less:         ['less/**/*.less'],
    scripts:      ['js/**/*.js']
  },
  dest    = './../',
  banner  = '/* Generated <%= (new Date()).toUTCString() %> */\n';

gulp.task('css', function(done) {
  
  // main css
  
  return gulp
    .src([
      'less/main.less'
    ])
    .pipe(less())
    .pipe(rename('optin.css'))
    .pipe(minifyCss())
    .pipe(header(banner))
    .pipe(gulp.dest(dest + 'css'))
    .pipe(livereload());    
        
});

gulp.task('scripts', function() {
    
  // main js

  return gulp
    .src(paths.scripts)
    .pipe(uglify())
    .pipe(header(banner))
    .pipe(gulp.dest(dest + 'js'))
    .pipe(livereload());
  
});

gulp.task('watch', function() {
  
  livereload.listen();

  gulp.watch('gulpfile.js', [ 'css', 'scripts' ]);
  gulp.watch(paths.less, ['css']);
  gulp.watch(paths.scripts, ['scripts']);
  
});

gulp.task('default', [ 'css', 'scripts', 'watch' ], function() {  
});